package application;
import vehicle.Bicycle;
public class BikeStore {
    public static void main(String[]args){
        //initializing the bicycle array
        Bicycle[] arrayBicycle = new Bicycle[4];
        // hard coding all of the values into the constructor
        arrayBicycle[0] = new Bicycle("Walmart", 15, 30);
        arrayBicycle[1] = new Bicycle("Target", 20, 40);
        arrayBicycle[2] = new Bicycle("Best Buy", 25, 45);
        arrayBicycle[3] = new Bicycle("Amazon", 30, 45);
        // printing out all of the value from the object 
        for(Bicycle objectName : arrayBicycle){
            System.out.println(objectName);
        }
    }
}
