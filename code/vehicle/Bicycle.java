package vehicle;
public class Bicycle {
    //fields
    private String manufacturer;
    private int numberGears;
    private double maxSpeed;
    //constructor
    public Bicycle(String manufacturer, int numberGears, double maxSpeed){
        this.manufacturer = manufacturer;
        this.numberGears = numberGears;
        this.maxSpeed = maxSpeed;
    }
    // to string
    public String toString(){
        String message = "Manufacturer: " + this.manufacturer + ", Number of Gears: " + this.numberGears + ", MaxSpeed: " + this.maxSpeed;
        return message; 
    }
    // getter methods
    public String getManufacturer(){
        return this.manufacturer;
    }
    public int getNumberGears(){
        return this.numberGears;
    }
    public double getMaxSpeed(){
        return this.maxSpeed;
    }

}
